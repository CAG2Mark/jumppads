package net.cubekrowd.jumppads;

import java.util.Objects;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class TypeDistanceCommand implements TypeCommand.Subcommand {
    private final JumpPadsPlugin plugin;

    public TypeDistanceCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "distance";
    }

    @Override
    public String getUsage() {
        return "<distance>";
    }

    @Override
    public String getDescription() {
        return "Set the jump distance";
    }

    @Override
    public void execute(CommandSender sender, JumpPadType type, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please enter the distance.");
            return;
        }

        double newDistance;
        try {
            newDistance = Double.parseDouble(args[0]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "That is not a valid distance.");
            return;
        }

        type.setDistance(newDistance);
        plugin.getConfig().set(type.getName() + ".distance", newDistance);
        plugin.saveConfig();
        sender.sendMessage(JumpPadsPlugin.MAIN_COLOUR + "Set the jump distance of \""
                + type.getName() + "\" to " + newDistance + ".");
    }
}
