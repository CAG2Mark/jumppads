package net.cubekrowd.jumppads;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class AboutCommand implements Subcommand {
    private final JumpPadsPlugin plugin;

    public AboutCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Displays plugin info.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var authors = new StringJoiner(", ");
        plugin.getDescription().getAuthors().forEach(authors::add);

        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + JumpPadsPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "] " + ChatColor.WHITE + "About the plugin:");
        joiner.add(ChatColor.GRAY + "- Authors: " + ChatColor.WHITE + authors);
        joiner.add(ChatColor.GRAY + "- Version: " + ChatColor.WHITE + plugin.getDescription().getVersion());
        joiner.add(ChatColor.GRAY + "- Website: " + ChatColor.WHITE + plugin.getDescription().getWebsite());

        sender.sendMessage(joiner.toString());
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}
