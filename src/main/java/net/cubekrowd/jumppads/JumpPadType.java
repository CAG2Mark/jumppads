package net.cubekrowd.jumppads;

import org.bukkit.Material;

public final class JumpPadType {
    private final String name;
    private final Material blockType;
    private double distance;
    private double height;

    public JumpPadType(String name, Material blockType, double distance,
            double height) {
        this.name = name;
        this.blockType = blockType;
        this.distance = distance;
        this.height = height;
    }

    public JumpPadType(String name, Material blockType) {
        this(name, blockType, 0, 0);
    }

    public String getName() {
        return name;
    }

    public Material getBlockType() {
        return blockType;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double newDistance) {
        distance = newDistance;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double newHeight) {
        height = newHeight;
    }
}
